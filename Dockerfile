FROM golang:1.21.4-alpine3.18 AS builder

WORKDIR /devops-app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY main.go .

RUN go build -o vehicles .



FROM builder AS tests-run

RUN go test -v ./...


FROM alpine:3.18.4 AS release

WORKDIR /devops-app

COPY --from=builder /devops-app/vehicles .


EXPOSE 8080

ENTRYPOINT [ "/devops-app/vehicles" ]





