#!/bin/bash

docker build . -t vehicles-app:v1.0.0.0

minikube image load vehicles-app:v1.0.0.0

kubectl apply -f ./infrastructure/k8s/deployment.yaml
